$(document).ready(function () {
	$('.js-slider').owlCarousel({
		loop: true,
		margin: 10,
		items: 1,
		nav: true,
		themeClass: 'slider-style-1'
	});
});

$(window).load(function () {
	// preloader
	// $('#status').fadeOut();
	// $('#preloader').delay(350).fadeOut('slow');
	// $('body').delay(350).css({
	// 	'overflow': 'visible'
	// });
});
//# sourceMappingURL=main.js.map
